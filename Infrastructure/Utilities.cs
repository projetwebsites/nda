using System;
using System.Globalization;
using System.Collections.Generic;
using NdaReservation.Models;

namespace NdaReservation.Infrastructure
{
    public static class Utilities
    {
        public static DateTime GetFirstDayOfTheMonth(int m){
            // on compose une date générique afin d'extraire le format du mois exacte de l'année en cours
            string s = DateTime.Today.Year.ToString() +"/"+m.ToString()+"/"+DateTime.Today.ToString("dd");
            DateTime dt = DateTime.ParseExact(s, "yyyy/M/d",CultureInfo.InvariantCulture);

            return new DateTime( DateTime.Today.Year, dt.Month, 1 );
        }
        
        public static DateTime GetLastDayOfTheMonth(int m)
        {
            return GetFirstDayOfTheMonth(m).AddMonths(1).AddDays(-1);
        }
        public static Dictionary<int, string> GetMonthsOfTheYear()
        {
            Dictionary<int, string> listOfMonths = new Dictionary<int, string>();
            listOfMonths.Add(1, "Janvier");
            listOfMonths.Add(2, "Février");
            listOfMonths.Add(3, "Mars");
            listOfMonths.Add(4, "Avril");
            listOfMonths.Add(5, "Mai");
            listOfMonths.Add(6, "Juin");
            listOfMonths.Add(7, "Juillet");
            listOfMonths.Add(8, "Aout");
            listOfMonths.Add(9, "Septembre");
            listOfMonths.Add(10, "Octobre");
            listOfMonths.Add(11, "Novembre");
            listOfMonths.Add(12, "Decembre");

            return listOfMonths;
        }
        public static DateTime GetMassTime(string h, DateTime d)
        {
            //converti les heures en format date normale
            DateTime t = DateTime.ParseExact(h, "HH:mm:ss", CultureInfo.InvariantCulture);

            return new DateTime(d.Year, d.Month, d.Day, t.Hour, t.Minute, t.Second);
        }
        // méthode // pour créer un code de reservation
        public static string SetReservationCode(string email, DateTime dt) 
        {
            return "nda" + email.GetHashCode().ToString() + dt.ToShortDateString();
        }
    }
}
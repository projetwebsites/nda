using System.Linq;

namespace NdaReservation.Models
{
    public class EFMassRepository:IMassRepository
    {
        private NdaDbContext context;
        public EFMassRepository(NdaDbContext ctx)
        {
            context = ctx;
        }
        public IQueryable<Mass> Messes => context.Messes;
        public void SaveMass(Mass messe)
        {
            if(messe.MassID == 0){
                context.Messes.Add(messe);
            }
            context.SaveChanges();
        }
    }
}
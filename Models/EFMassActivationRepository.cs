using System.Linq;

namespace NdaReservation.Models
{
    public class EFMassActivationRepository : IMassActivationRepository
    {
        private NdaDbContext m_context;
        public EFMassActivationRepository(NdaDbContext ctx)
        {
            m_context = ctx;
        }
        public IQueryable<MassActivation> ActiveMasses => m_context.ActiveMasses;
        public void ActivateMass(MassActivation masseToActivate)
        {
            if(masseToActivate.MassActivationID == 0){
                m_context.ActiveMasses.Add(masseToActivate);
            }
            m_context.SaveChanges();
        }
    }
}
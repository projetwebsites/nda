using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace NdaReservation.Models
{
    public class MassActivation
    {
        [BindNever]
        public long MassActivationID { get; set; }
        [BindNever]
        public DateTime MassDateTime { get; set; }
        [BindNever]
        public bool IsOpened { get; set; }
    }
}
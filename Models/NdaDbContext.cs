using Microsoft.EntityFrameworkCore;

namespace NdaReservation.Models
{
    public class NdaDbContext : DbContext
    {
        public NdaDbContext(DbContextOptions<NdaDbContext> options) : base(options) {}

        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Mass> Messes { get; set; }
        public DbSet<MassActivation> ActiveMasses { get; set; }
    }
}

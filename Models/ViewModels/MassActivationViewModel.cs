using System.Collections.Generic;

namespace NdaReservation.Models.ViewModels
{
    public class MassActivationViewModel
    {
        public IEnumerable<MassActivation> MassesToActivate { get; set; }
        public IDictionary<int, string> ListOfMonths { get; set; }
    }
}
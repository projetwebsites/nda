using System.Linq;

namespace NdaReservation.Models
{
    public interface IReservationRepository
    {
        IQueryable<Reservation> Reservations { get;}
        void SaveReservation(Reservation reservation);
    }
}
namespace NdaReservation.Models
{    
    public class Mass
    {
        public long MassID { get; set; }
        //-public DayID DayId {get; set;}
        public string  DayOfTheWeek{ get; set; }
        public string HourOfCelebration {get; set;}
        public bool IsOpened {get; set;}
    }
}
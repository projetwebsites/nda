using System.Linq;

namespace NdaReservation.Models
{
    public interface IMassActivationRepository
    {
        IQueryable<MassActivation> ActiveMasses{ get; }// take all masses activated or not
        void ActivateMass(MassActivation masseToActivate);
    }
}
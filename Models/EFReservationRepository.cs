using System.Linq;

namespace NdaReservation.Models
{
    public class EFReservationRepository:IReservationRepository
    {
        private NdaDbContext context;

        public EFReservationRepository(NdaDbContext ctx){
            context = ctx;
        }
        public IQueryable<Reservation> Reservations => context.Reservations;
        public void SaveReservation(Reservation reservation)
        {
            if(reservation.ReservationID == 0){
                context.Reservations.Add(reservation);
            }
            context.SaveChanges();
        }
    }
}
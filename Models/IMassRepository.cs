using System.Linq;

namespace NdaReservation.Models
{
    public interface IMassRepository
    {
        IQueryable<Mass> Messes { get; }
        void SaveMass(Mass mess);
    }
}
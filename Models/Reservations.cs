using System;
using System.ComponentModel.DataAnnotations;

namespace NdaReservation.Models{
    public class Reservation{
        public long ReservationID { get; set; }
        public string ReservationCode { get; set; }

        [Required(ErrorMessage = "SVP, Veuillez entrer votre Prénom")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "SVP, Veuillez entrer votre Nom de famille")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "SVP, Veuillez entrer votre Numéro de téléphone")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-]?([0-9]{3})[-]?([0-9]{4})$", ErrorMessage="Le format du numéro est incorrecte")]
        //, ErrorMessage = "The PhoneNumber field is not a valid phone number"
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "SVP, Veuillez entrer votre email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "SVP, Veuillez choisir le jour de votre participation")]
        public DateTime MassDate { get; set; }
        public bool HasABubble { get; set; }
    }
}
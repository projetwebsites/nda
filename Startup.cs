using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using NdaReservation.Models;

namespace NdaReservation
{
    public class Startup
    {
        private IConfiguration Configuration { get; set; }
        public Startup(IConfiguration config)
        {
            Configuration = config;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            //Ajouter la configuration de la db
            services.AddDbContext<NdaDbContext>(options => {
                options.UseSqlServer(Configuration["ConnectionStrings:NdaReservationConnection"]);
                //options.UseSqlServer(Configuration["Data:NdaMassReservation:ConnectionString"]);
            });
            services.AddScoped<IReservationRepository, EFReservationRepository>();
            services.AddScoped<IMassRepository, EFMassRepository>();
            services.AddScoped<IMassActivationRepository, EFMassActivationRepository>();
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //env.EnvironmentName ="Production";
            if(env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }
            app.UseDeveloperExceptionPage();
            app.UseStatusCodePages();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseEndpoints(endpoints => {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}

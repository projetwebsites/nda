using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using NdaReservation.Models;
using NdaReservation.Infrastructure;


namespace NdaReservation.Controllers{
    public class HomeController: Controller
    {
        private IReservationRepository m_reservationRepository;
        private IMassActivationRepository m_massActivationRepository;

        public HomeController(IReservationRepository resRepo, IMassActivationRepository massActivationRepo)
        {
            m_reservationRepository = resRepo;
            m_massActivationRepository = massActivationRepo;
        }
        public IActionResult Index()
        {
            // Renvoyer le nombre des messes disponibles pour la reservation
            return View();
        }
        [HttpGet]
        public ViewResult ReservationForm()
        {
            List<MassActivation> massTimeList = new List<MassActivation>();
            massTimeList = m_massActivationRepository.ActiveMasses.Where(mt =>(mt.MassDateTime.Date >= DateTime.Today.Date) && 
                                                                              (mt.MassDateTime.Date <= DateTime.Today.Date.AddDays(14) &&
                                                                               mt.IsOpened == true)).ToList();
            ViewBag.ListOfMassTime = massTimeList;
            return View(new Reservation());
        }
        [HttpPost]
        public ViewResult ReservationForm(Reservation reservation)
        {
            if(ModelState.IsValid)
            {
                Reservation _reserve = reservation;
                _reserve.ReservationCode = Utilities.SetReservationCode(_reserve.Email, _reserve.MassDate);

                if(!reservation.HasABubble){
                    m_reservationRepository.SaveReservation(_reserve);
                    return View("Thanks", _reserve );
                }
                else{
                    m_reservationRepository.SaveReservation(_reserve);
                    return View("BubleThanks", _reserve);
                }
            }
            else
            {
                List<MassActivation> massTimeList = new List<MassActivation>();
                massTimeList = m_massActivationRepository.ActiveMasses.Where(mt =>(mt.MassDateTime.Date >= DateTime.Today.Date) && 
                                                                                (mt.MassDateTime.Date <= DateTime.Today.Date.AddDays(14) &&
                                                                                mt.IsOpened == true)).ToList();
                ViewBag.ListOfMassTime = massTimeList;
                return View(reservation);
            }
        }
        //
        [HttpGet]
        public ViewResult BubleReservationForm(string reservationCode)
        {
            if(!string.IsNullOrEmpty(reservationCode)){
                // on créé l'objet de la prochaine réservation
                Reservation _nextReservation = m_reservationRepository.Reservations.Where(c => c.ReservationCode.Equals(reservationCode)).First();
                return View(_nextReservation);
            }
            else{
                return View("Index");
            }
        }
        //
        [HttpPost]
        public ViewResult BubleReservationForm(Reservation reservation)
        {
            if(ModelState.IsValid){
                m_reservationRepository.SaveReservation(reservation);
                return View("BubleThanks", reservation);
            }
            else{
                return View(reservation);
            }
        }
        //montrer la liste de ceux qui ont déjà reservé
        [HttpGet]

        public ViewResult ListeReservations(long selectedDay )
        {
            List<MassActivation> massTimeList = new List<MassActivation>();
            int currentMonth = (int)DateTime.Today.Month;
            massTimeList = m_massActivationRepository.ActiveMasses.Where(mt =>(mt.MassDateTime.Date >= DateTime.Today.Date) && 
                                                                              (mt.MassDateTime.Date <= Utilities.GetLastDayOfTheMonth(currentMonth) &&
                                                                               mt.IsOpened == true)).ToList();
            ViewBag.ListOfMassTime = massTimeList;
            ViewBag.SelectedDay= selectedDay;
            
            if( selectedDay == 0 ){
                //ViewBag.SelectedDay= selectedDay;
                return View(m_reservationRepository.Reservations.Where(mr => (mr.MassDate.Date >= DateTime.Today.Date) && 
                                                                            ( mr.MassDate.Date <= Utilities.GetLastDayOfTheMonth(currentMonth))));
            }
            List<MassActivation> getActiveMassDateTime = new List<MassActivation>();
            getActiveMassDateTime = m_massActivationRepository.ActiveMasses.Where(am => (am.MassActivationID == selectedDay)).ToList();
            return View(m_reservationRepository.Reservations.Where(mt => (mt.MassDate == getActiveMassDateTime[0].MassDateTime)));
        }
    }
}
using Microsoft.AspNetCore.Mvc;

namespace NdaReservation.Controllers
{
    public class ErrorController : Controller
    {
        public ViewResult Error() => View();
    }
}
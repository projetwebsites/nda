using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using NdaReservation.Models;
using NdaReservation.Models.ViewModels;
using NdaReservation.Infrastructure;


namespace NdaReservation.Controllers
{
    public class MassActivationController : Controller
    {
        private IMassRepository m_massRepository;
        private IMassActivationRepository m_massActivationRepository;
        private NdaDbContext m_applicationContex;

        public MassActivationController(
            IMassRepository massRepository, 
            IMassActivationRepository massActivationRepository,
            NdaDbContext ctx
        ){
            m_massRepository = massRepository;
            m_massActivationRepository = massActivationRepository;
            m_applicationContex = ctx;
        }

        public ViewResult Index() // on affiche le tableau du mois avec des messes activées ou non
        {
            return View( new MassActivationViewModel {
                MassesToActivate = m_massActivationRepository.ActiveMasses,
                ListOfMonths = Utilities.GetMonthsOfTheYear()
            });
        }

        [HttpPost]
        public IActionResult ActivateMass(int MonthToActivate) // on affiche le tableau du mois avec des messes activées ou non
        {
            if(ModelState.IsValid)
            {
                DateTime firstday = Utilities.GetFirstDayOfTheMonth(MonthToActivate);
                DateTime lastday = Utilities.GetLastDayOfTheMonth(MonthToActivate);
                do
                {
                    foreach(var mass in m_massRepository.Messes)
                    {
                        if(mass.DayOfTheWeek.ToLower() == firstday.ToString("dddd", new System.Globalization.CultureInfo("fr-FR")))
                        {
                            m_applicationContex.ActiveMasses.AddRange(
                                new MassActivation {
                                    MassDateTime = Utilities.GetMassTime(mass.HourOfCelebration, firstday),
                                    IsOpened = true
                                }
                            );
                        }
                    }
                    m_applicationContex.SaveChanges();
                    firstday = firstday.AddDays(1);
                }while(firstday <= lastday);
                return RedirectToAction(nameof(Details));
            }
            return View("Index");
        }

        [HttpGet]
        public ViewResult Details(int selectedMonth)
        {
            if( selectedMonth == 0 ){ // || selectedMonth == DateTime.Today.Month get the default month of the current year{
                
                //TempData["m"] = DateTime.Today.Month;
                ViewBag.Selection = DateTime.Today.Month;
                return View( new MassActivationViewModel {

                    MassesToActivate = m_massActivationRepository.ActiveMasses.Where( m =>
                                                                m.MassDateTime.Month == DateTime.Today.Month),
                    ListOfMonths = Utilities.GetMonthsOfTheYear()
                });
            }
            else{ // get view for a specific month

                ViewBag.Selection = selectedMonth;
                return View( new MassActivationViewModel {

                    MassesToActivate = m_massActivationRepository.ActiveMasses.Where( m =>
                                                                m.MassDateTime.Month == selectedMonth),
                    ListOfMonths = Utilities.GetMonthsOfTheYear()
                });
            }

        }
    }
}
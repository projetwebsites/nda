using Microsoft.AspNetCore.Mvc;
using NdaReservation.Models;

namespace NdaReservation.Controllers
{
    public class MassController : Controller
    {
        private IMassRepository m_massRepository;

        public MassController(IMassRepository massRepository)
        {
            m_massRepository = massRepository;
        }
        public ViewResult Index() // on affiche les messes déjà créé
        {
            return View(m_massRepository.Messes);
        }
        
        [HttpGet]
        public IActionResult Create() => View();
        //public IActionResult Create() => View("Edit", new Mass());
        
        [HttpPost]
        public IActionResult Create(Mass messe)
        {
            if(ModelState.IsValid)
            {
                // on enregistre la messe créé
                m_massRepository.SaveMass(messe);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View(messe);//donne moi l'accord / promesse
            }
        }
        // création du reste des méthodes
        [HttpGet]
        public IActionResult Edit(int massID)
        {
            return View("Index");
        }
        [HttpGet]
        public IActionResult Delete(int massID)
        {
            return View("Index");
        }
    }
}